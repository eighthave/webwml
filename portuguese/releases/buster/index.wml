#use wml::debian::template title="Informações de lançamento do Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f"

<p>O Debian <current_release_buster> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
  "O Debian 10.0 foi inicialmente lançado em <:=spokendate('2019-07-06'):>."
/>
O lançamento incluiu muitas mudanças importantes, descritas no
nosso <a href="$(HOME)/News/2019/20190706">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 10 foi substituído pelo
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
#Atualizações de segurança foram descontinuadas em <:=spokendate('xxxx-xx-xx'):>.
</strong></p>

### Este parágrafo é orientativo, revise antes de publicar!
#<p><strong>Contudo, o buster se beneficia do suporte de longo prazo (LTS -
#Long Term Support) até o final de xxxxx 20xx. O LTS é limitado ao i386, amd64,
#armel, armhf e arm64.
#Todas as outras arquiteturas não são mais suportadas no buster.
#Para mais informações, consulte a <a
#href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
#</strong></p>

<p>Para obter e instalar o Debian, veja a página <a
href="debian-installer/">informações de instalação</a> e o
<a href="installmanual">Guia de Instalação</a>. Para atualizar a partir de uma
versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

###  Ative o seguinte, quando o período LTS começar.
#<p>Arquiteturas suportadas durante o suporte de longo prazo:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>
# <p>Computer architectures supported at initial release of buster:</p> ### Use this line when LTS starts, instead of the one above.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Ao contrário do que desejamos, pode haver alguns problemas existentes na
versão, mesmo que ela seja declarada <em>estável (stable)</em>. Nós fizemos
<a href="errata">uma lista dos principais problemas conhecidos</a>, e
você sempre pode nos
<a href="reportingbugs">relatar outros problemas</a>.</p>

<p>Por último, mas não menos importante, nós temos uma lista de <a
href="credits">pessoas que merecem crédito</a> por fazer este
lançamento acontecer.</p>

