#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10686">CVE-2017-10686</a>

<p>Dans Netwide Assembler (NASM) version 2.14rc0, il existe plusieurs
vulnérabilités d’utilisation de mémoire de tas après libération dans l’outil
nasm. Le tas concerné est alloué dans la fonction token() et libéré dans la
fonction detoken() (appellée par pp_getline()). Il est utilisé de nouveau dans
plusieurs positions pouvant causer plusieurs dommages. Par exemple, il provoque
une liste corrompue doublement liée dans detoken(), une double libération de
zone de mémoire ou une corruption dans delete_Token() et une écriture hors
limites dans detoken(). Il est hautement probable que cela aboutisse à une
attaque d’exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11111">CVE-2017-11111</a>

<p>Dans Netwide Assembler (NASM) version 2.14rc0, preproc.c permet à des
attaquants distants de provoquer un déni de service (dépassement de tampon basé
sur le tas et plantage d'application) ou éventuellement d’avoir un impact non
précisé à l'aide d'un fichier contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.10.01-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nasm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1041.data"
# $Id: $
