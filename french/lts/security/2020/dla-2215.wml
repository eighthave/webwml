#use wml::debian::translation-check translation="e1f2faa7586d93bcc8b80311dd121544b49b388f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été trouvés dans le paquet src:clamav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3327">CVE-2020-3327</a>

<p>Une vulnérabilité dans le module d’analyse d’archive ARJ dans Clam AntiVirus
(ClamAV) pourrait permettre à un attaquant distant non authentifié de
provoquer une condition de déni de service pour un périphérique visé. La
vulnérabilité est due à une lecture hors limites de tampon de tas. Un attaquant
pourrait exploiter cette vulnérabilité en envoyant un fichier ARJ contrefait au
périphérique visé. Un exploit pourrait permettre à l’attaquant de provoquer le
plantage du processus d’analyse de ClamAV, aboutissant à une condition de déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3341">CVE-2020-3341</a>

<p>Une vulnérabilité dans le module d’analyse d’archive PDF dans Clam AntiVirus
(ClamAV) pourrait permettre à un attaquant distant non authentifié de
provoquer une condition de déni de service pour un périphérique visé. La
vulnérabilité est due à une lecture hors limites de tampon de pile. Un attaquant
pourrait exploiter cette vulnérabilité en envoyant un fichier PDF contrefait au
périphérique visé. Un exploit pourrait permettre à l’attaquant de provoquer le
plantage du processus d’analyse de ClamAV, aboutissant à une condition de déni
de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.101.5+dfsg-0+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2215.data"
# $Id: $
