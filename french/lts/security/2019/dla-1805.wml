#use wml::debian::translation-check translation="d845a712913b094bfe0c14d00924ffcd2de2e2c7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité d’utilisation de mémoire après libération dans
minissdpd, un démon de découverte de périphériques réseau. Un attaquant distant
pourrait utiliser cela pour faire planter le processus.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12106"></a>

<p>La fonction updateDevice dans minissdpd.c dans MiniUPnP MiniSSDPd 1.4 et 1.5
permet à un attaquant distant de faire planter le processus à cause d’une
vulnérabilité d’utilisation après libération.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.2.20130907-3+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets minissdpd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1805.data"
# $Id: $
