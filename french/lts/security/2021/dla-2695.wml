#use wml::debian::translation-check translation="99ab03c59de19e7d92335d61f458e109514df581" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans klibc. Selon la
manière dont klibc est utilisée, elles pourraient conduire à l'exécution de
code arbitraire, à une élévation des privilèges ou à un déni de service.</p>

<p>Merci à Microsoft Vulnerability Research pour le signalement des bogues
de tas et pour son approche dans l’identification des bogues de cpio.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31870">CVE-2021-31870</a>

<p>La multiplication dans la fonction calloc() peut aboutir à un
dépassement d'entier et à un dépassement consécutif de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31871">CVE-2021-31871</a>

<p>Un dépassement d'entier dans la commande cpio peut aboutir à un
déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31872">CVE-2021-31872</a>

<p>Plusieurs dépassements possibles d'entier dans la commande cpio sur les
systèmes 32 bits peuvent conduire à un dépassement de tampon ou avoir un
autre impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31873">CVE-2021-31873</a>

<p>Les additions dans la fonction malloc() peuvent aboutir à un
dépassement d'entier et à un dépassement consécutif de tampon de tas.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0.4-9+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets klibc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de klibc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/klibc">\
https://security-tracker.debian.org/tracker/klibc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2695.data"
# $Id: $
