<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13776">CVE-2017-13776</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-13777">CVE-2017-13777</a>

      <p>denial of service issue in ReadXBMImage()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12935">CVE-2017-12935</a>

      <p>The ReadMNGImage function in coders/png.c mishandles large MNG
      images, leading to an invalid memory read in the
      SetImageColorCallBack function in magick/image.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12936">CVE-2017-12936</a>

      <p>The ReadWMFImage function in coders/wmf.c has a use-after-free
      issue for data associated with exception reporting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12937">CVE-2017-12937</a>

      <p>The ReadSUNImage function in coders/sun.c has a colormap
      heap-based buffer over-read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13063">CVE-2017-13063</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-13064">CVE-2017-13064</a>

      <p>heap-based buffer overflow vulnerability in the function
      GetStyleTokens in coders/svg.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13065">CVE-2017-13065</a>

      <p>NULL pointer dereference vulnerability in the function
      SVGStartElement in coders/svg.c</p>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u9.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1082.data"
# $Id: $
