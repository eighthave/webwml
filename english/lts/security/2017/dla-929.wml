<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several heap-based buffer overflows, integer overflows and NULL pointer
dereferences have been discovered in libpodofo, a library for
manipulating PDF files, that allow remote attackers to cause a denial
of service (application crash) or other unspecified impact via a
crafted PDF document.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9.0-1.1+deb7u1.</p>

<p>We recommend that you upgrade your libpodofo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-929.data"
# $Id: $
