<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in phpMyAdmin, the web-based MySQL
administration interface, including SQL injection attacks, denial of
service, arbitrary code execution, cross-site scripting, server-side
request forgery, authentication bypass, and file system traversal.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4:4.2.12-2+deb8u3.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1415.data"
# $Id: $
