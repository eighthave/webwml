<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability in the email parsing module in Clam AntiVirus
(ClamAV) Software version 0.103.1 and all prior versions could
allow an unauthenticated, remote attacker to cause a denial of
service condition on an affected device. The vulnerability is
due to improper variable initialization that may result in an
NULL pointer read. An attacker could exploit this vulnerability
by sending a crafted email to an affected device. An exploit
could allow the attacker to cause the ClamAV scanning process
crash, resulting in a denial of service condition.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.102.4+dfsg-0+deb9u2.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>For the detailed security status of clamav please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/clamav">https://security-tracker.debian.org/tracker/clamav</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2626.data"
# $Id: $
