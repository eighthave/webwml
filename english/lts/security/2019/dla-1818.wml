<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Joe Vennix discovered an authentication bypass vulnerability in dbus, an
asynchronous inter-process communication system. The implementation of
the DBUS_COOKIE_SHA1 authentication mechanism was susceptible to a
symbolic link attack. A local attacker could take advantage of this flaw
to bypass authentication and connect to a DBusServer with elevated
privileges.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.22-0+deb8u2.</p>

<p>We recommend that you upgrade your dbus packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1818.data"
# $Id: $
