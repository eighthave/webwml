<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Integer signedness error in the simplestring_addn function in
simplestring.c in xmlrpc-epi through 0.54.2 allows remote attackers to
cause a denial of service (heap-based buffer overflow) or possibly have
unspecified other impact via a long first argument to the PHP
xmlrpc_encode_request function.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.54.2-1+deb7u1.</p>

<p>We recommend that you upgrade your xmlrpc-epi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-569.data"
# $Id: $
