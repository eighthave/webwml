#use wml::debian::template title="Reasons to Choose Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"

<p>There are a lot of reasons why users choose Debian as their operating system.</p>

<h1>Major Reasons</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian is Free software.</strong></dt>
  <dd>
    Debian is made of free and open source software and will always be 100%
    <a href="free">free</a>. Free for anyone to use, modify, and
    distribute. This is our main promise to <a href="../users">our users</a>. It's also free of cost.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is a stable and secure Linux based operating system.</strong></dt>
  <dd>
    Debian is an operating system for a wide range of devices including
    laptops, desktops and servers. Users like its stability and reliability
    since 1993. We provide reasonable default configuration for every package.
    The Debian developers provide security updates for all packages over their
    lifetime whenever possible.
  </dd>
</dl>

<dl>
  <dt><strong>Debian has extensive hardware support.</strong></dt>
  <dd>
    Most hardware is already supported by the Linux kernel. Proprietary drivers
    for hardware are available when the free software is not sufficient.
  </dd>
</dl>

<dl>
  <dt><strong>Debian provides smooth upgrades.</strong></dt>
  <dd>
    Debian is well known for its easy and smooth upgrades within a release cycle
    but also to the next major release.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is the seed and base for many other distributions.</strong></dt>
  <dd>
    Many of the most popular Linux distributions, like Ubuntu, Knoppix, PureOS,
    SteamOS or Tails, choose Debian as a base for their software.
    Debian is providing all the tools so everyone can extend the software
    packages from the Debian archive with their own packages for their needs.
  </dd>
</dl>

<dl>
  <dt><strong>The Debian project is a community.</strong></dt>
  <dd>
    Debian is not only a Linux operating system. The software is co-produced by
    hundreds of volunteers from all over the world. You can be part of the
    Debian community even if you are not a programmer or sysadmin. Debian is
    community and consensus driven and has a
    <a href="../devel/constitution">democratic governance structure</a>.
    Since all Debian developers have equal rights it cannot be controlled by
    a single company. We have developers in more than 60 countries and support
    translations for our Debian Installer in more than 80 languages.
  </dd>
</dl>

<dl>
  <dt><strong>Debian has multiple installer options.</strong></dt>
  <dd>
    End users will use our
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
    including the easy-to-use Calamares installer needing very little input or
    prior knowledge. More experienced users can use our unique full featured
    installer while experts can fine tune the installation or even use an
    automated network installation tool.
  </dd>
</dl>

<br>

<h1>Enterprise Environment</h1>

<p>
  If you need Debian in a professional environment, you may enjoy these
  additional benefits:
</p>

<dl>
  <dt><strong>Debian is reliable.</strong></dt>
  <dd>
    Debian proves its reliability every day in thousands of real world
    scenarios ranging from a single user laptop up to super colliders, stock
    exchanges and the automotive industry. It's also popular in the academic
    world, in science and the public sector.
  </dd>
</dl>

<dl>
  <dt><strong>Debian has many experts.</strong></dt>
  <dd>
    Our package maintainers do not only take care of the Debian packaging and
    incorporating new upstream versions. Often they're experts in the upstream
    software and contribute to upstream development directly. Sometimes they
    are also part of upstream.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is secure.</strong></dt>
  <dd>
    Debian has security support for its stable releases. Many other
    distributions and security researchers rely on Debian's security tracker.
  </dd>
</dl>

<dl>
  <dt><strong>Long Term Support.</strong></dt>
  <dd>
    There's <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS) at no charge. This brings you extended support for the stable
    release for 5 years and more. Besides there's also the
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a> initiative
    that extends supports for a limited set of packages for more than 5 years.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud images.</strong></dt>
  <dd>
    Official cloud images are available for all major cloud platforms. We also
    provide the tools and configuration so you can build your own customized
    cloud image. You can also use Debian in virtual machines on the desktop or
    in a container.
  </dd>
</dl>

<br>

<h1>Developers</h1>
<p>Debian is widely used by every kind of software and hardware developers.</p>

<dl>
  <dt><strong>Public available bug tracking system.</strong></dt>
  <dd>
    Our Debian <a href="../Bugs">Bug tracking system</a> (BTS) is publicly
    available for everybody via a web browser. We do not hide our software
    bugs and you can easily submit new bug reports.
  </dd>
</dl>

<dl>
  <dt><strong>IoT and embedded devices.</strong></dt>
  <dd>
    We support a wide range of devices like the Raspberry Pi, variants of QNAP,
    mobile devices, home routers and a lot of Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Multiple Hardware Architectures.</strong></dt>
  <dd>
    Support for a <a href="../ports">long list</a> of CPU architectures
    including amd64, i386, multiple versions of ARM and MIPS, POWER7, POWER8,
    IBM System z, RISC-V. Debian is also available for older and specific
    niche architectures.
  </dd>
</dl>

<dl>
  <dt><strong>Huge number of software packages available.</strong></dt>
  <dd>
    Debian has the largest number of installed packages available
    (currently <packages_in_stable>). Our packages use the deb format which is well
    known for its high quality.
  </dd>
</dl>

<dl>
  <dt><strong>Different choices of releases.</strong></dt>
  <dd>
    Besides our stable release, you get the newest versions of software
    by using the testing or unstable releases.
  </dd>
</dl>

<dl>
  <dt><strong>High quality with the help of developer tools and policy.</strong></dt>
  <dd>
    Several developer tools help to keep the quality to a high standard and
    our <a href="../doc/debian-policy/">policy</a> defines the technical
    requirements that each package must satisfy to be included in the
    distribution. Our continuous integration is running the autopkgtest
    software, piuparts is our installation, upgrading, and removal testing
    tool and lintian is a comprehensive package checker for Debian packages.
  </dd>
</dl>

<br>

<h1>What our users say</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      For me it's the perfect level of ease of use and stability. I've used
      various different distributions over the years but Debian is the only one
      that just works.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Rock solid. Loads of packages. Excellent community.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian for me is symbol of stability and ease to use.
    </strong></q>
  </li>
</ul>

