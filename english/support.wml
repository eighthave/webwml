#use wml::debian::template title="Support"
#use wml::debian::toc

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian and its support are run by a community of volunteers.

If this community-driven support doesn't fulfil your needs, you may
read our <a href="doc/">documentation</a> or
hire a <a href="consultants/">consultant</a>.

<toc-display />

<toc-add-entry name="irc">On-line Real Time Help Using IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> is a way
to chat with people from all over the world in real time.
IRC channels dedicated to Debian can be found on
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>To connect, you need an IRC client. Some of the most popular clients are
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> and
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
all of which have been packaged for
Debian. OFTC also offers a <a href="https://www.oftc.net/WebChat/">WebChat</a>
web interface which allows you to connect to IRC with a browser without
the need to install any local client.</p>

<p>Once you have the client installed, you need to tell it to connect
to the server. In most clients, you can do that by typing:</p>

<pre>
/server irc.debian.org
</pre>

<p>In some clients (such as irssi) you will need to type this instead:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Once you are connected, join channel <code>#debian</code> by typing</p>

<pre>
/join #debian
</pre>

<p>Note: clients like HexChat often have a different, graphical user interface
for joining servers/channels.</p>

<p>At this point you will find yourself among the friendly crowd of
<code>#debian</code> inhabitants. You're welcome to ask questions about
Debian there. You can find the channel's faq at 
<url "https://wiki.debian.org/DebianIRC" />.</p>


<p>There's a number of other IRC networks where you can chat about Debian,
too.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Mailing lists</toc-add-entry>

<p>Debian is developed through distributed development all around
the world. Therefore e-mail is a preferred way to discuss various items.
Much of the conversation between Debian developers and users is managed
through several mailing lists.</p>

<p>There are several publicly available mailing lists. For more information,
see <a href="MailingLists/">Debian mailing lists</a> page.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>
For user support in English, please contact the 
<a href="https://lists.debian.org/debian-user/">debian-user
mailing list</a>.
</p>

<p>
For user support in other languages, please check the
<a href="https://lists.debian.org/users.html">mailing
lists index for users</a>.
</p>

<p>There are of course many other mailing lists, dedicated to some aspect
of the vast Linux ecosystem, which are not Debian-specific. Use your
favorite search engine to find the most suitable list for your purpose.</p>


<toc-add-entry name="usenet">Usenet newsgroups</toc-add-entry>

<p>A lot of our <a href="#mail_lists">mailing lists</a> can be browsed as
newsgroups, in the <kbd>linux.debian.*</kbd> hierarchy. This can also be
done using a web interface such as
<a href="https://groups.google.com/forum/">Google Groups</a>.


<toc-add-entry name="web">Web sites</toc-add-entry>

<h3 id="forums">Forums</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal on which
you can discuss Debian-related topics, submit questions about Debian, and have
them answered by other users.</p>


<toc-add-entry name="maintainers">Reaching Package Maintainers</toc-add-entry>

<p>There are two ways of reaching package maintainers. If you need to
contact the maintainer because of a bug, simply file a bug report (see the
Bug Tracking System section below). The maintainer will get a copy of the
bug report.</p>

<p>If you simply want to communicate with the maintainer, then you can use
the special mail aliases set up for each package. Any mail sent to
&lt;<em>package name</em>&gt;@packages.debian.org will be forwarded to the
maintainer responsible for that package.</p>


<toc-add-entry name="bts" href="Bugs/">The Bug Tracking System</toc-add-entry>

<p>The Debian distribution has a bug tracking system which
details bugs reported by users and developers.  Each bug is given a
number, and is kept on file until it is marked as having been dealt
with.</p>

<p>To report a bug, you can read the bug page below; we recommend the
use of the Debian package <q>reportbug</q> to automatically file a bug report.</p>

<p>Information on submitting bugs, viewing the currently active bugs, and
the bug tracking system in general can be found at the
<a href="Bugs/">bug tracking system web pages</a>.</p>


<toc-add-entry name="consultants" href="consultants/">Consultants</toc-add-entry>

<p>Debian is free software and offers free help through mailing lists. Some
people either don't have the time or have specialized needs and are willing
to hire someone to maintain or add additional functionality to their Debian
system.  See the <a href="consultants/">consultants page</a> for a list
of people/companies.</p>


<toc-add-entry name="release" href="releases/stable/">Known problems</toc-add-entry>

<p>Limitations and severe problems of the current stable distribution
(if any) are described on <a href="releases/stable/">the release pages</a>.</p>

<p>Pay particular attention to the <a href="releases/stable/releasenotes">release
notes</a> and the <a href="releases/stable/errata">errata</a>.</p>
