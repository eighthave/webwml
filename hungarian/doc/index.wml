#use wml::debian::template title="Felhasználói dokumentáció"
#use wml::debian::translation-check translation="9aa7cfecde4026c09b63f9d42fa74cbe0f7e50bb"  maintainer="Szabolcs Siebenhofer"
# translated by Szabolcs Siebenhofer <the7up@gmail.com> maintainer="Szabolcs Siebenhofer"

<p>Minden operációs rendszernek fontos része a dokumentáció, a műszaki leírás, ami leírja 
a programok működését és használatát. Annak az erőfeszítésnek a részeként, hogy magas 
szinvonaló operációs rendszert hozzunk létre, a Debian projekt mindent megtesz annak érdekében,
hogy minden felhasználóját számára megfelelő, könnyen elérhető dokumentációhoz juttassa.

<h2>Gyorstalpaló</h2>

<p>Ha <em>új</em> neked a Debian, akkor azt javasoljuk, ezek elolvasásával
kezdd:</p>

<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Telepítési kézikönyv</a>
<li><a href="manuals/debian-faq/">Debian GNU/Linux GYIK</a>
</ul>

<p>Az első Debian telepítésekor legyenek kéznél, valószínűleg sok kérdésedre kapsz 
belőlük választ, és segítenek az új Debian rendszereddel való munkában. Később ezeket 
érdemes átnézned:</p>

<ul>
  <li><a href="manuals/debian-handbook/">A Debian Adminisztrátorok kézikönyve</a>, az átfogó 
    felhasználói leírás</li>
  <li><a href="manuals/debian-reference/">Debian Hivatkozások</a>, a rövidített felhasználói 
    útmutató, aminek középpontjában a shell parancssor áll
  <li><a href="$(HOME)/releases/stable/releasenotes">Verzióinformációk</a>, azok
    számára, akik frissítenek
  <li><a href="https://wiki.debian.org/">Debian Wiki</a>, értékes információforrás
    az új felhasználók számára
</ul>


<p>Végül mindenképp érdemes kinyomtatnod, és a kezed ügyében tartanod a
<a href="https://www.debian.org/doc/manuals/refcard/refcard">Debian GNU/Linux
referenciakártyát</a>, a Debian rendszerek legfontosabb parancsainak listáját.</p>

<p>Ezen kívül további dokumentációkat is találhatsz az alábbi listában.</p>

<h2>Dokumentációk típusai</h2>

<p>A legtöbb dokumentáció, amit a Debian tartalmaz, általában a
GNU/Linux-hoz íródott. Szintén megtalálható benne néhány Debian-specifikus
leírás. A dokumentumok az alábbi kategóriákba sorolhatók:</p>

<ul>
   <li><a href="#manuals">kézikönyvek</a>
   <li><a href="#howtos">HOWTO-k (hogyanok)</a>
   <li><a href="#faqs">FAQ-k (GYIK-ok)</a>
   <li><a href="#other">más rövid dokumentumok</a>
</ul>

<h3 id="manuals">Kézikönyvek</h3>

<p>A kézikönyvek valódi könyvekre hasonlítanak, mivel kimerítően tárgyalnak
egy-egy nagyobb témát.

<h3>Debian-specifikus kézikönyvek</h3>

<div class="line">
  <div class="item col50">

    <h4>Felhasználói kézikönyvek</h4>
    <ul>
    <li><a href="user-manuals#faq">Debian GNU/Linux FAQ</a></li>
    <li><a href="user-manuals#install">Debian Installation Guide</a></li>
    <li><a href="user-manuals#relnotes">Debian Release Notes</a></li>
    <li><a href="user-manuals#refcard">Debian Reference Card</a></li>
    <li><a href="user-manuals#debian-handbook">The Debian Administrator's Handbook</a></li>
    <li><a href="user-manuals#quick-reference">Debian Reference</a></li>
    <li><a href="user-manuals#securing">Securing Debian Manual</a></li>
    <li><a href="user-manuals#aptitude">aptitude user's manual</a></li>
    <li><a href="user-manuals#apt-guide">APT User's Guide</a></li>
    <li><a href="user-manuals#apt-offline">Using APT Offline</a></li>
    <li><a href="user-manuals#java-faq">Debian GNU/Linux and Java FAQ</a></li>
    <li><a href="user-manuals#hamradio-maintguide">Debian Hamradio Maintainer’s Guide</a></li>
        </ul>

  </div>

  <div class="item col50 lastcol">

    <h4>Fejlesztői kézikönyvek</h4>
    <ul>
    <li><a href="devel-manuals#policy">Debian Policy Manual</a></li>
    <li><a href="devel-manuals#devref">Debian Developer's Reference</a></li>
    <li><a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
    <li><a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a></li>
    <li><a href="devel-manuals#packaging-tutorial">Introduction to Debian packaging</a></li>
    <li><a href="devel-manuals#menu">Debian Menu System</a></li>
    <li><a href="devel-manuals#d-i-internals">Debian Installer internals</a></li>
    <li><a href="devel-manuals#dbconfig-common">Guide for database using package maintainers</a></li>
    <li><a href="devel-manuals#dbapp-policy">Policy for packages using databases</a></li>
    </ul>
    
    <h4>Egyéb kézikönyvek</h4>
    <ul>
    <li><a href="misc-manuals#history">Debian Project History</a></li>
    </ul>

  </div>


</div>

<p class="clr">A Debian kézikönyveinek és más dokumentációjának teljes listája
megtalálható a <a href="ddp">Debian dokumentációs projekt</a> weboldalán.</p>

<p>Néhány, a felhasználók kezébe szánt Debian GNU/Linuxhoz írt
kézikönyv elérhető a <a href="books">nyomtatott könyvek</a> oldalon.

<h3 id="howtos">HOWTO-k</h3>

<p>A <a href="http://www.tldp.org/docs.html#howto">HOWTO dokumentumok</a> 
(magyarul HOGYANok), ahogy a nevük is mutatja, arról szólnak, <em>hogyan</em> csinálj 
dolgokat, és alaposan kitárgyalják az adott kérdést.</p>


<h3 id="faqs">FAQ-k</h3>

<p>A FAQ (magyarul GYIK) jelentése: <em>gyakran ismétlődő
kérdések</em>. A FAQ egy olyan doksi, ami válaszol az ottani kérdésekre.</p>

<p>A Debiant érintő kérdésekre a <a href="manuals/debian-faq/">Debian FAQ</a>-ban
találhatod meg a választ.  Külön <a href="../CD/faq/">FAQ</a> áll rendelkezésre
a Debian CD/DVD-képfájlokról.</p>


<h3 id="other">Más, rövid dokumentumok</h3>

<p>A következő leírások rövidebb, gyorsabb útmutatást tartalmaznak:

<dl>

  <dt><strong><a href="http://www.tldp.org/docs.html#man">kézikönyvoldalak</a></strong></dt>
    <dd>A hagyományok szerint minden Unix program dokumentációja
    elérhető <em>kézikönyvoldalakon</em>, amiket a <tt>man</tt>
    paranccsal lehet elérni, de ezeket általában nem kezdőknek szánják. A
    Debianhoz elérhető kézikönyvoldalakat olvasni és tartalmukban keresni a 
    <a href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>
    címen lehet.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info fájlok</a></strong></dt>
    <dd>Sok GNU szoftver dokumentációja <em>info fájlokban található</em>
    kézikönyvoldalak helyett. Ezek a fájlok a program és  lehetőségei
    részletes leírását, illetve a használatra vonatkozó példákat
    tartalmaznak, és az <tt>info</tt> parancs segítségével érhetők el.
    </dd>

  <dt><strong>különböző README fájlok</strong></dt>
    <dd>A <em>read me (olvass el)</em> fájlok szintén gyakoriak, ezek
    egyszerű szövegfájlok, amik általában egy csomag tartalmát írják
    le. Ezek legtöbbjét a Debian-rendszereden a
    <tt>/usr/share/doc/</tt> könyvtár alkönyvtáraiban találhatod meg.
    Ebben a könyvtárban minden programcsomagnak saját alkönyvtára van a
    hozzá tartozó read me fájlokkal, ami tartalmazhat a beállításra
    vonatkozó példákat is. Ne felejtsd el, hogy a nagyobb programok
    dokumentációja általában külön csomagban található (aminek a neve
    megegyezik az eredeti csomagéval, <em>-doc</em> végződéssel).
    
  <dt><strong>gyorsreferencia-kártyák</strong></dt>
  <dd>
    <p>A gyorsreferencia-kártyák egy (al)rendszer leírásának tömör összefoglalását
    nyújtják. Általában a leggyakrabban használt
    parancsokat tartalmazzák egyetlen papírlapra nyomtatott formában. Néhány
    említésre méltó referenciakártya, illetve gyűjtemény:</p>
    <dl>
      <dt><a href="https://www.debian.org/doc/manuals/refcard/refcard">Debian GNU/Linux referenciakártya</a></dt>
    <dd> Ez a kártya, ami kinyomtatható egyetlen lapra, a legfontosabb parancsok
  listáját és a hozzájuk tartozó leírásokat tartalmazzak, amik segítségével
  a kezdő Debian felhasználók alaposabban megismerkedhetnek velük.
  Használatához a számítógépek, a fájlok, a könyvtárak és a parancssor
  fogalmának legalább alapszintű ismerete szükséges. Teljesen kezdő
  felhasználók számára először a <a
  href="user-manuals#quick-reference">Debian referencia</a> elolvasása
  ajánlott.</dd>

</dl>
  </dd>
</dl>

<hrline />

<p>Ha a fenti oldalak mindegyikét átnézted és még mindig nem kaptál
választ a kérdéseidre, vagy megoldást a Debian-nak kapcsolatos problémádra, 
akkor látogasd meg a <A HREF="../support">terméktámogatási oldalunkat</A>.
