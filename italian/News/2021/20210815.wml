<define-tag pagetitle>Debian Edu / Skolelinux Bullseye — una soluzione Linux completa per la tua scuola</define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="e7ce9859ec2794a8c0d0978182d2d513ed96cfa0" maintainer="Davide Prina"

<p>
Sei l'amministratore di un laboratorio di computer o dell'intera rete di una
scuola?
Vorresti installare server, workstation e portatili in modo che possano
funzionare insieme?
Vuoi la stabilità di Debian con i servizi di rete già preconfigurati?
Vuoi avere uno strumento web per gestire sistemi e diverse centinaia o più
account utenti?
Ti sei chiesto se e come vecchi computer possono essere usati?
</p>

<p>
Allora Debian Edu è adatto per te. Gli stessi insegnanti, o il loro supporto
tecnico, possono installare un ambiente completo di studio multi-utente e
multi-PC in pochi giorni. Debian Edu è fornito con centinaia di applicazioni
preinstallate e si possono sempre aggiungere ulteriori pacchetti da Debian.
</p>

<p>
Il team di sviluppo di Debian Edu è felice di annunciare Debian Edu 11
<q>Bullseye</q>, il rilascio Debian Edu / Skolelinux basato sul rilascio di
Debian 11 <q>Bullseye</q>.
Per favore, considera la possibilità di aiutarci a migliorare ulteriormente
Debian Edu, testando e fornendoci resoconti
(&lt;debian-edu@lists.debian.org&gt;).
</p>

<h2>Informazioni su Debian Edu e Skolelinux</h2>

<p>
<a href="https://blends.debian.org/edu"> Debian Edu, conosciuto anche come
Skolelinux</a>, è una distribuzione Linux basata su Debian che fornisce un
ambiente di base di una rete scolastica completamente configurato.
Immediatamente dopo l'installazione, è configurato un server scolastico, con
in esecuzione tutti i servizi necessari ad una rete scolastica, in attesa
che utenti e computer vengano aggiunti tramite GOsa², un'accogliente
interfaccia web.
Un ambiente di boot dalla rete è preparato, così che dopo l'installazione
iniziale del server principale sia possibile installare, tramite la rete, tutte
le altre macchine usando CD / DVD / BD o penna USB.
I computer più vecchi (anche fino a circa 10 anni fa) possono essere usati come
LTSP thin client o workstation senza disco, facendo il boot dalla rete senza
nessuna installazione o configurazione.
Il server scolastico Debian Edu fornisce una base dati LDAP e servizi di
autenticazione Kerberos, directory home centralizzate, un server DHCP, un proxy
web e molti altri servizi. L'ambiente desktop contiene più di 70 pacchetti
software educativi e altri sono disponibili dagli archivi Debian.
Le scuole possono scegliere tra gli ambienti desktop: Xfce, GNOME, LXDE,
MATE, KDE Plasma, Cinnamon e LXQt.
</p>

<h2>Nuove funzionalità in Debian Edu 11 <q>Bullseye</q></h2>

<p> Questi sono alcuni elementi dalle note di rilascio di Debian Edu 11
<q>Bullseye</q>, basate sul rilascio di Debian 11 <q>Bullseye</q>.
La lista completa, che include informazioni più dettagliate, fa parte del
relativo
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">capitolo del manuale di Debian Edu</a>.
</p>

<ul>
<li>
Novità <a href="https://ltsp.org">LTSP</a> per gestire workstation senza disco.
I thin client sono ancora supportati tramite l'uso della tecnologia <a href="https://wiki.x2go.org">X2Go</a>.
</li>
<li>
L'Avvio dalla rete è fornito tramite iPXE, al posto di PXELINUX, per essere
conforme con LTSP.
</li>
<li>
La modalità d'installazione grafica di Debian è utilizzata per installazioni
iPXE.
</li>
<li>
Samba è ora configurato come <q>server indipendente</q> con supporto per
SMB2/SMB3.
</li>
<li>
DuckDuckGo è utilizzato come motore di ricerca predefinito sia in Firefox ESR
che in Chromium.
</li>
<li>
Sono stati aggiunti nuovi strumenti per configurare freeRADIUS con supporto di
entrambi i metodi EAP-TTLS/PAP e PEAP-MSCHAPV2.
</li>
<li>
Sono disponibili strumenti migliorati per configurare un nuovo sistema con
profilo <q>Minimale</q> come gateway dedicato.
</li>
</ul>

<h2>Opzioni per il download, passi per l'installazione e manuale</h2>

<p>
Sono disponibili ufficialmente le immagini dei CD di Debian Network-Installer
per PC sia a 64-bit che a 32-bit. L'immagine a 32-bit sarà necessaria soltanto
in casi rari (per vecchi PC che hanno circa 15 anni). Le immagini possono
essere scaricate dai seguenti indirizzi:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Sono anche disponibili, in alternativa, immagini BD ufficiali di Debian
(occupano più di 5 GB). È possibile configurare un'intera rete Debian Edu senza
una connessione internet (inclusi tutti gli ambienti desktop e localizzazioni
per tutte le lingue gestite da Debian).
Queste immagini sono disponibili ai seguenti indirizzi:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Le immagini possono essere verificate usando i checksum firmati forniti nella
directory di download.
<br />
Una volta scaricata l'immagini è possibile verificare che
</p>
<ul>
<li>
il suo checksum corrisponda a quanto atteso dal file di checksum; e che
</li>
<li>
il file di checksum non sia stato alterato.
</li>
</ul>
<p>Per maggiori informazioni, su come eseguire questi passi, leggere la
<a href="https://www.debian.org/CD/verify">guida di verifica</a>.
</p>

<p>
Debian Edu 11 <q>Bullseye</q> è interamente basato su Debian 11 <q>Bullseye</q>;
per questo motivo i sorgenti di tutti i pacchetti sono disponibili dagli archivi
di Debian.
</p>

<p>
Per favore notate la 
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">pagina di stato di Debian Edu Bullseye</a>.
per informazioni sempre aggiornate per Debian Edu 11 <q>Bullseye</q>, incluse
le istruzioni su come fare il <code>rsync</code> per scaricare le immagini ISO.
</p>

<p>
Quando si aggiorna da Debian Edu 10 <q>Buster</q> per favore leggere il
relativo
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">capitolo del manuale di Debian Edu</a>.
</p>

<p>
Per le note di installazione leggere il relativo
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">capitolo del manuale di Debian Edu</a>.
</p>

<p>
Dopo l'installazione è necessario eseguire questi
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">primi passi</a>.
</p>

<p>
Per favore vedere le <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">pagine del wiki di Debian Edu</a>
per l'ultima versione del manuale inglese di Debian Edu <q>Bullseye</q>.
Il manuale è stato completamente tradotto in: tedesco, francese, italiano,
danese, olandese, norvegese Bokmål, giapponese, cinese semplificato e
portoghese (Portogallo).
Versioni di traduzione parziali esistono per: spagnolo, rumeno e polacco.
È disponibile un sommario dell'
<a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
ultima versione pubblicata del manuale</a>.
</p>

<p>
Maggiori informazioni circa Debian 11 <q>Bullseye</q> sono fornite nelle
note di rilascio e nel manuale di installazione; vedere <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Informazioni su Debian</h2>

<p>Il progetto Debian è un'associazione di sviluppatori di Software Libero che
volontariamente donano il loro tempo e impegno per produrre un sistema
operativo completamente libero.
</p>

<h2>Informazione per i contatti</h2>

<p>Per maggiori informazioni, visitare le pagine web di Debian
<a href="$(HOME)/">https://www.debian.org/</a> o inviare una email a
&lt;press@debian.org&gt;.</p>

