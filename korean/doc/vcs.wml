#use wml::debian::ddp title="데비안 문서 프로젝트 VCS"
#use wml::debian::toc
#use wml::debian::translation-check translation="bf1a0486dfbf35e11a7ff98a29d9e2e4f2eda3f3" maintainer="Sebul" 

<p>데비안 문서 프로젝트는 웹 페이지와 많은 매뉴얼 텍스트를 <strong>salsa.debian.org</strong>에 있는 데비안 살사 서비스에 저장하는데, 이는 데비안의 gitlab 인스턴스입니다.
이 서비스가 어떻게 동작하는지 자세한 정보는 <a href="https://wiki.debian.org/Salsa">salsa 문서</a>를 읽으세요.
</p>

<p>누구나 Salsa 서비스에서 소스를 받을 수 있습니다.
데비안 문서 프로젝트 회원만 파일을 업데이트 할 수 있습니다.</p>

<toc-display />

<toc-add-entry name="access">git에서 소스에 접근하기</toc-add-entry>

<p>웹 인터페이스를 사용하여 파일에 개별적으로 접근하고 각 프로젝트의 변경 사항을 <url "https://salsa.debian.org/ddp-team/" />에서 볼 수 있습니다</p>

<p>전체 매뉴얼을 받으려면, git 서버에 직접 접근하는 게 때로 좋은 선택입니다. 컴퓨터에 <tt><a href="https://packages.debian.org/git">git</a></tt> 패키지가 필요합니다.</p>

<h3>git 저장소를 익명(읽기전용)으로 복제</h3>

<p>이 명령을 써서 한 프로젝트 모든 파일을 받습니다:</p>

<p style="margin-left: 2em">
  <code>git clone https://salsa.debian.org/ddp-team/release-notes.git</code>
</p>

<p>로컬에 복제할 모든 프로젝트에 같은 작업을 하세요.</p>

<h3>push 권한(읽기-쓰기)을 갖고 git 저장소 복제</h3>

<p>git서버에 이 방법으로 접근하기 전에 git 서버 쓰기 권한을 받아야 합니다. push 권한을 어떻게 <a
href="#obtaining">요청</a>하는지 먼저 읽으세요.</p>

<p>이 명령을 써서 한 프로젝트 모든 파일을 받습니다:</p>

<p style="margin-left: 2em">
  <code>git clone git@salsa.debian.org:ddp-team/release-notes.git</code>
</p>

<p>로컬에 복제할 모든 프로젝트에 같은 작업을 하세요.</p>

<h3>원격 git 저장소로부터 변경 페치</h3>

<p>다른 사람이 바꾼 것으로 로컬 사본을 업데이트 하려면,  <strong>manuals</strong> 디렉터리로 들어가서 명령 실행:</p>

<p style="margin-left: 2em">
  <code>git pull</code>
</p>

<toc-add-entry name="obtaining">push 권한 얻기</toc-add-entry>

<p>push 권한은 매뉴얼 작성에 참여하고자 하는 모든 사람이 이용할 수 있습니다. 일반적으로 유용한 패치를 먼저 제출해야 합니다.
</p>

<p><a href="https://salsa.debian.org/">Salsa</a>에 계정을 만들 다음,
<url "https://salsa.debian.org/ddp-team/" /> 그룹 또는 아래 
<q>Request to join</q>을 클릭해서 push 권한을 요청하세요.
데비안에 대한 당신의 작업 기록을 설명하는 메일을 debian-doc@lists.debian.org에 보내고 그것을 따라가십시오.
</p>

<p>요청을 한번 승인 받으면, <a
href="https://salsa.debian.org/ddp-team/"><q>ddp-team</q> 그룹</a> 또는 그 프로젝트 중 하나의 한 부분이 될 겁니다.
</p>

<hr />

<toc-add-entry name="updates">자동 업데이트 매커니즘</toc-add-entry>

<p>매뉴얼 텍스트의 게시된 웹 페이지는 www.master.debian.org에서 일반 웹 사이트 재 구축 프로세스의 일부로 생성되며, 4시간마다 발생합니다.</p>

<p>프로세스는 아카이브에서 최신 패키지 버전을 다운로드하고 각 매뉴얼를 다시 빌드하고
웹 사이트의 <code>doc/manuals/</code> 하위 디렉터리에 있 파일을 설치하도록 설정됩니다.</p>

<p>업데이트 스크립트로 생성된 문서 파일은
<a href="manuals/">https://www.debian.org/doc/manuals/</a>에 있습니다.</p>

<p>업데이트 스크립트 파일이 만든 로그 파일은
<url "https://www-master.debian.org/build-logs/webwml/" />
(스크립트 이름은 <code>7doc</code>이며 <code>often</code> cron job의 일부로 실행됨).
