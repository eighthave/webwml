#use wml::debian::template title="왜 데비안을 선택하는가" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Sebul"

<p>여러 이유로 사용자는 데비안을 그들의 운영체제로 선택합니다.</p>

<h1>주요 이유</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>데비안은 자유 소프트웨어 입니다.</strong></dt>
  <dd>
  데비안은 자유 오픈 소스 소프트웨어로 만들며 항상 100% <a href="free">free</a>입니다. 
  누구나 사용, 수정 및 배포할 수 있습니다.
  이것이 <a href="../users">사용자</a>에게 약속하는 주요 사항입니다.
  가격도 무료입니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안정적이고 안전한 리눅스 기반 운영체제입니다.</strong></dt>
  <dd>
  데비안은 랩톱, 데스크톱 및 서버를 비롯한 광범위한 장치를 위한 운영 체제입니다. 
  사용자들은 1993년 이후로 안정성과 신뢰성을 좋아했습니다. 모든 패키지에 대해 적절한 기본 구성을 제공합니다. 
  데비안 개발자는 가능한 한 평생 동안 모든 패키지에 대한 보안 업데이트를 제공합니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 넓은 하드웨어를 지원합니다.</strong></dt>
  <dd>
  대부분의 하드웨어를 리눅스 커널에서 이미 지원합니다. 
  하드웨어 전용 드라이버는 무료 소프트웨어가 충분하지 않을 때 사용할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 부드러운 업그레이드를 지원합니다.</strong></dt>
  <dd>
  데비안은 릴리스 주기 내에 쉽고 매끄럽게 업그레이드되는 것으로 잘 알려져 있으며, 
  다음 주요 릴리스로 업그레이드할 수도 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 다른 많은 배포판의 씨앗이며 기반입니다.</strong></dt>
  <dd>
  Ubuntu, Knoppix, PureOS, SteamOS 또는 Tails 같이 가장 많이 사용되는 리눅스 배포판 중 상당수는 소프트웨어 기반으로 데비안을 선택합니다. 
  데비안은 모든 도구를 제공하므로 누구든지 필요에 따라 데비안 아카이브의 소프트웨어 패키지를 자신의 패키지로 확장할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안 프로젝트는 커뮤니티입니다.</strong></dt>
  <dd>
  데비안은 리눅스 운영체제일 뿐만이 아닙니다. 
이 소프트웨어는 전 세계 수백 명의 자원봉사자가 함께 만들었습니다. 
프로그래머나 sysadmin이 아니더라도 데비안 커뮤니티 일원이 될 수 있습니다. 
데비안은 공동체적이고 합의 지향적이며 <a href="../devel/constitution">민주적인 통치 구조</a>를 가집니다. 
모든 데비안의 개발자들은 동등한 권리를 가지고 있기 때문에 그것을 단일 회사가 통제할 수 없습니다. 
60개 이상의 국가에 개발자가 있으며 80개 이상의 언어로 데비안 설치관리자 번역을 지원합니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 여러 설치 옵션이 있습니다.</strong></dt>
  <dd>
    최종 사용자는 입력이나 사전 지식이 거의 필요하지 않은 사용하기 쉬운 Calamares 설치 프로그램을 포함하여 <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">라이브 CD</a>를 사용합니다. 
    경험이 많은 사용자는 당사의 고유한 전체 기능 설치 프로그램을 사용할 수 있으며, 전문가들은 설치를 세부적으로 조정하거나 자동화된 네트워크 설치 도구도 사용할 수 있습니다.
  </dd>
</dl>

<br>

<h1>기업환경</h1>

<p>
데비안이 프로페셔널 환경에서 필요하면 다음 추가 혜택을 누릴 수 있습니다.
</p>

<dl>
  <dt><strong>데비안은 믿을 수 있습니다.</strong></dt>
  <dd>
  데비안은 단일 사용자 노트북에서 수퍼 콜리더, 증권거래소 및 자동차 산업에 이르는 수천 개의 실제 시나리오에서 매일 신뢰성을 입증하고 있습니다.
그것은 또한 학계와 과학계와 공공부문에서도 인기가 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 전문가가 많습니다.</strong></dt>
  <dd>
  당사의 패키지 관리자들은 데비안 패키징과 새로운 업스트림 버전을 통합하는 것뿐만 아니라 종종 그들은 업스트림 소프트웨어의 전문가이며 업스트림 개발에 직접 기여합니다.
  때때로 그들은 업스트림의 일부이기도 합니다.
  </dd>
</dl>

<dl>
  <dt><strong>데비안은 안전합니다.</strong></dt>
  <dd>
  데비안은 안정 릴리스를 위해 보안 지원을 받고 있습니다. 
  다른 많은 유통 및 보안 연구자들은 데비안의 보안 추적기에 의존하고 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>장기 지원.</strong></dt>
  <dd>
<a href="https://wiki.debian.org/LTS">장기 지원 (LTS)</a> 무료. 
이를 통해 안정적인 릴리스에 대한 지원이 5년 이상 연장됩니다.
또한 제한된 패키지 세트에 대한 지원을 5년 이상 연장하는 <a href="https://wiki.debian.org/LTS/Extended">확장 
LTS</a> 이니셔티브도 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>클라우드 이미지.</strong></dt>
  <dd>
  공식 클라우드 이미지는 모든 주요 클라우드 플랫폼에서 쓸 수 있습니다. 
  또한 맞춤형 클라우드 이미지를 구축 할 수 있도록 도구와 구성도 제공합니다. 
  데스크톱 또는 컨테이너의 가상 머신에서도 데비안을 쓸 수 있습니다.
  </dd>
</dl>

<br>

<h1>개발자</h1>
<p>
데비안은 모든 종류의 소프트웨어와 하드웨어 개발자들이 널리 사용합니다.
</p>

<dl>
  <dt><strong>공개 버그 추적 시스템 사용 가능.</strong></dt>
  <dd>
  데비안 <a href="../Bugs">버그 추적 시스템</a>(BTS)은 웹 브라우저를 통해 누구나 공개적으로 사용할 수 있습니다. 
  우리는 소프트웨어 버그를 숨기지 않으며 새 버그 보고서를 쉽게 제출할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>IoT 및 임베디드 장비.</strong></dt>
  <dd>
라즈베리 파이, QNAP의 변형, 모바일 장치, 홈 라우터 및 많은 SBC(싱글 보드 컴퓨터) 같은 광범위한 장치를 지원합니다.
  </dd>
</dl>

<dl>
  <dt><strong>다중 하드웨어 아키텍처.</strong></dt>
  <dd>
  amd64, i386, ARM 및 MIPS의 여러 버전, POWER7, POWER8, IBM System z, RISC-V를 포함한 <a href="../ports">긴 CPU 아키텍처 목록</a> 지원. 
  데비안은 구식 및 특정 틈새 아키텍처에서도 사용할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>수많은 소프트웨어 사용 가능.</strong></dt>
  <dd>
  데비안에는 가장 많은 수의 설치된 패키지(현재 <packages_in_stable> 개)가 있습니다.
  우리 패키지는 높은 품질로 잘 알려진 deb 포맷을 씁니다.
  </dd>
</dl>

<dl>
  <dt><strong>다른 릴리스 선택.</strong></dt>
  <dd>
안정 릴리스 외에도 테스트 또는 불안정 릴리스를 사용하여 최신 버전의 소프트웨어를 이용할 수 있습니다.
  </dd>
</dl>

<dl>
  <dt><strong>개발자 도구 및 정책 도움으로 고품질.</strong></dt>
  <dd>
  여러 개발자 도구는 품질을 높은 수준으로 유지하는 것을 돕고, 구글 <a href="../doc/debian-policy/">정책</a>은 각 패키지가 배포에 포함되기 위해 충족해야 하는 기술 요구 사항을 정의합니다.
지속적인 통합은 autopkgtest 소프트웨어를 실행하고 있으며 piuparts는 설치, 업그레이드 및 제거 테스트 도구이며, lintian은 데비안 패키지에 대한 포괄적인 패키지 검사기입니다.
  </dd>
</dl>

<br>

<h1>사용자들이 말하는 것</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
    사용의 용이성과 안정성의 완벽한 수준입니다. 
    수년 동안 다양한 배포판을 사용해 왔지만 데비안만이 동작하는 유일한 배포판입니다.
    </strong></q>
  </li>

  <li>
    <q><strong>
      바위처럼 단단함. 많은 패키지. 훌륭한 커뮤니티.
    </strong></q>
  </li>

  <li>
    <q><strong>
    데비안은 안정성과 사용 편의성의 상징입니다.
    </strong></q>
  </li>
</ul>
