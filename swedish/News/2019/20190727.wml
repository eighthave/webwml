<define-tag pagetitle>DebConf19 avslutas i Curitiba och datum för DebConf20 tillkännages</define-tag>
<define-tag release_date>2019-07-27</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794"
#use wml::debian::news

<p>
Idag, lördagen 27 juli 2019, avslutades den årliga Debiankonferensen
för för utvecklare och bidragslämnare. Med mer än 300 deltagare från
50 olika länder, med 145 evenemang inklusive föredrag, diskussionssessioner,
Birds of a Feather-möten (BoF), workshops och aktiviteter, hyllas
<a href="https://debconf19.debconf.org">DebConf19</a> som en succé.
</p>

<p>
Konferensen föregicks av det årliga DebCamp som hölls 14 juli till 19 juli
med fokus på individuellt arbete och grupparbeten för personligt
samarbete på utveckling av Debian men är även värd för en 3-dagars
workshop för paketering där ni bidragslämnare påbörjar sitt arbete
med Debianpaketering.</p>

<p>
<a href="https://debconf19.debconf.org/news/2019-07-20-open-day/">Open Day</a>
hölls 20 juli, med mer än 250 besökare som beskådade presentationer och
workshops med bred målgrupp, en jobbmässa med bås från flera av sponsorerna av
DebConf19 samt en Debianinstallationsfest.
</p>

<p>
Den faktiska konferensen startade söndagen 21 juli 2019. Tillsammas med plenarsessioner
som den traditionella Bits from the DPL, blixtföredrag och livedemos samt
tillkännagivandet av nästa års DebConf
(<a href="https://wiki.debian.org/DebConf/20">DebConf20</a> i Haifa, Israel),
har det varit flera sessioner relaterade till den senaste utgåvan av
Debian 10 Buster och några av dess nya funktioner, nyhetsuppdateringar om flera
projekt och interna rupper inom Debian, diskussionssessioner (BoF's) för språkgrupper,
anpassningar, infrastruktur och gemenskapsgrupper, och många andra intressanta
evenemang om Debian och fri mjukvara.
</p>

<p>
<a href="https://debconf19.debconf.org/schedule/">Schemat</a>
har uppdaterats varje dag, inklusive ad-hoc-aktiviteter, introducerade
av deltagare under hela konferensen.
</p>

<p>
För dem som inte kunde besöka konferensen spelades de flesta föredrag och
sessioner in och liveströmmades, och videos görs tillgängliga på
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2019/DebConf19/">arkivwebbsidan för Debianmöten</a>.
Nästan alla underlättade även fjärrdeltagande via IRC eller kollaborativa
textdokument.
</p>

<p>
<a href="https://debconf19.debconf.org/">DebConf19-webbsidan</a>
kommer att vara aktiv för arkiveringsändamål, och kommer att fortsätta
erbjuda länkar till presentationer och videos av föredrag och evenemang.
</p>

<p>
Nästa år kommer <a href="https://wiki.debian.org/DebConf/20">DebConf20</a> att
hållas i Haifa, Israel, från 23 augusti till 29 augusti 2020.
Enligt traditionen kommer de lokala organisatörerna att före DebConf sätta upp
DebCamp (16 augusti till 22 augusti), med särskilt fokus på individuellt- och
teamarbete för att förbättra distributionen.
</p>

<p>
DebConf strävar efter en säker och välkomnande miljö för alla deltagare.
Under konferensen finns flera team (receptions-, välkomstteamet, och
Antimobbingsteamet) tillgängliga för att hjälpa så att deltagare både
på plats och fjärrdeltagare får den bästa erfarenheten av konferensen, och
finner lösningar på alla problem som kan uppkomma.
Se <a href="https://debconf19.debconf.org/about/coc/">webbsidan om förhållningsregler på DebConf19-webbplatsen</a>
för ytterligare detaljer rörande detta.
</p>

<p>
Debian tackar för åtagandet från flera <a href="https://debconf19.debconf.org/sponsors/">sponsorer</a>
för deras stöd för DebConf19, speciellt från Platinasponsorer:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
och <a href="https://www.lenovo.com">Lenovo</a>.
</p>

<h2>Om Debian</h2>

<p>
Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
en stor mängd datortyper, kallar sig Debian det <q>universella 
operativsystemet</q>.
</p>

<h2>Om DebConf</h2>

<p>
DebConf är Debianprojektets utvecklarkonferens. Utöver ett fullt schema med
tekniska, sociala och policyföreläsningar, ger DebConf en möjlighet för
utvecklare, bidragslämnare och andra intresserade att mötas personligen
och jobba närmare tillsammans. Det har ägt rum årligen sedan 2000 på
så vitt skiljda platser som Skottland, Argentina, Bosnien och Hercegovina.
Mer information om DebConf finns tillgänglig på 
<url http://debconf.org>.
</p>

<h2>Om Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> är Schweiz största webbhostingföretag,
som även erbjuder backup- och lagringstjänster, lösningar för evenemangsorganisatörer,
liveströmmande och video-on-demand-tjänster.
Det äger helt sina datacenter och alla kritiska element för funktionen av
de tjänster och produkter som företaget erbjuder (både mjukvara och hårdvara).
</p>

<h2>Om Google</h2>

<p>
<a href="https://google.com/">Google</a> är ett av världens största teknologiföretag,
som tillhandahåller ett brett spektrum av Internet-relaterade tjänster och produkter
så som onlinereklamteknologi, sökning, molntjänster, mjukvara och hårdvara.
</p>

<p>
Google har supportat Debian genom att sponsra DebConf i mer än tio år,
och är även en Debianpartner som sponsrar delar av 
<a href="https://salsa.debian.org">Salsa</a>'s continuous integration-infrastruktur
med Google Cloud Plattformen.
</p>

<h2>Om Lenovo</h2>

<p>
Som en globla teknologiledare som tillverkar en bred portfolio av anslutna enheter,
inklusive mobiltelefoner, tablets, PCs och arbetsstationer så väl som AR/VR-enheter,
smarta hem/kontor och datacenterlösningar, förstår <a href="https://www.lenovo.com">Lenovo</a>
hur kritiska öppna system och plattformar är för en ansluten värld.
</p>

<h2>Kontaktinformation</h2>

<p>För mer information, besök vänligen DebConf19 webbplats på
<a href="https://debconf19.debconf.org/">https://debconf19.debconf.org/</a>
eller skicka e-post (på engelska) till &lt;press@debian.org&gt;.</p>
