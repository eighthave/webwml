#use wml::debian::template title="Как помочь в работе с веб-страницами Debian" BARETITLE=true
#use wml::debian::translation-check translation="e64f14dabbc209061f6ad3b317236189aa702271" maintainer="Lev Lamberov"

<h3>Введение</h3>

<p>Спасибо за ваш интерес к работе над веб-сайтом Debian. По причине большого
размера Debian и из-за того, что все разработчики являются добровольцами, мы
разбили большие задачи на более управляемые части. Большая часть работы по
сопровождению веб-сайта не требует навыков программирования, так что мы можем
использовать таланты почти любого компьютерного энтузиаста.</p>

<p>Человеку, как-либо участвующему в работе над веб-сайтом, стоит
<a href="https://lists.debian.org/debian-www/">подписаться на список
рассылки debian-www</a>. Координаторы перевода <strong>обязаны</strong>
подписаться на него. Все обсуждения, касающиеся веб-сайта, проводятся там.</p>

<p>Куда вам отправится далее, зависит от того, чем вы интересуетесь:

<ul>
  <li><a href="desc">описание того, как организован веб-сайт</a>&nbsp;&mdash;
    <em>настоятельно рекомендуется прочитать!</em></li>
  <li><a href="working">создание или редактирование страниц на английском языке</a>
    <ul>
      <li><a href="using_git">как использовать git</a></li>
      <li><a href="using_wml">как использовать WML</a></li>
      <li><a href="htmlediting">как использовать HTML в веб-страницах Debian</a></li>
      <li><a href="todo">список основных задач</a></li>
    </ul>
  <li><a href="translating">перевод страниц</a>
    <ul>
      <li><a href="content_negotiation">согласование содержания</a></li>
      <li><a href="uptodate">поддержание переводов в актуальном состоянии</a></li>
      <li><a href="translation_hints">советы переводчикам</a></li>
      <li><a href="examples">некоторые примеры</a></li>
      <li><a href="stats">статистика по переводам</a></li>
      <li><a href="translation_coordinators">список координаторов перевода</a></li>
    </ul>

</ul>
