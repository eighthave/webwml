#use wml::debian::template title="Hjælp/support"
#use wml::debian::translation-check translation="76ce7e7f5cfd0e2f3e8931269bdcd1f9518ee67f"
#use wml::debian::toc

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<p>Debian og dets support drives af et fællesskab af frivillige.
Hvis denne fællesskabsdrevne support ikke opfylder dine behov, kan du prøve at 
læse vores <a href="doc/">dokumentation og wiki</a> eller ansætte en 
<a href="consultants/">konsulenter</a>.</p>

<toc-display />

<toc-add-entry name="irc">Direkte hjælp på IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) 
benyttes til direkte kommunikation mellem mennesker over hele verden. 
IRC-kanaler om Debian findes på <a href="https://www.oftc.net/">OFTC</a>.</p>

<p>For at deltage i en sådan chat, skal man bruge et IRC-klientprogram. Nogle 
af de mest populære klienter er
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> og
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
der alle findes som Debian-pakker.  OFTC tilbyder også en 
<a href="https://www.oftc.net/WebChat/">WebChat</a>, dvs. et webinterface, som 
gør det muligt at benytte IRC med en webbrowser, uden behov for at have en lokal 
klient.</p>

<p>Når man har installeret klienten, skal man
bede den om at forbinde sig med serveren.  I de fleste klienter kan man 
skrive:</p>

<pre>
/server irc.debian.org
</pre>

<p>I nogle klienter (eksempelvis irssi) skal du i stedet skrive:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

# Added futher below for Danish, as the channel is very small.

<p>Når man har opnået forbindelse, skal man tilslutte sig den engelsksprogede kanal
<code>#debian</code> ved at skrive:</p>

<pre>
  /join #debian
</pre>

<p>Bemærk: Klienter som HexChat har ofte en anderledes, grafisk brugergræseflade
til brug ved tilslutning til servere og kanaler.</p>

<p>Man befinder sig nu blandt en flok venlige <code>#debian</code>-folk, som 
man er velkommen til på engelsk at stille spørgsmål om Debian.  Kanalens OSS er 
tilgængelig på <url "https://wiki.debian.org/DebianIRC" />.</p>

<p>Der er også en lille dansksproget IRC-kanal, <code>#debian.dk</code>.</p>

<p>Desuden er der andre IRC-netværk, hvor du kan chatte om Debian.</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Postlister</toc-add-entry>

<p>Debian er udviklet gennem en distribueret indsats fra hele verden. 
Derfor er e-mail en foretrukken måde at diskutere forskellige emner på.  Meget 
af kontakten mellem Debians udviklere og brugere håndteres ved hjælp af 
adskillige postlister.</p>

<p>Der er adskillige offentligt tilgængelige postlister.  Se 
<a href="MailingLists/">Debians postlistesider</a> for flere oplysninger.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<p>For hjælp på dansk, kan postlisten 
<a href="https://lists.debian.org/debian-user-danish/">debian-user-danish</a> 
anvendes, på engelsk findes listen 
<a href="https://lists.debian.org/debian-user/">debian-user</a>.</p>

<p>Det er selvfølgelig mange andre Linux-postlister, rettet mod specifikke 
aspekter ved det omfattende Linux-økosystem, og som ikke er Debian-specifikke.  
Anvend din foretrukne søgemaskine til at finde den mest velegnede liste til dit 
formål.</p>


<toc-add-entry name="usenet">Usenet-nyhedsgrupper</toc-add-entry>

<p>Mange af vore <a href="#mail_lists">postlister</a> kan man kigge i som 
nyhedsgrupper, i hierarkiet <kbd>linux.debian.*</kbd>.  Dette kan også gøres med
en webgrænseflade som <a href="https://groups.google.com/forum/">Google Groups</a>.</p>


<toc-add-entry name="web">Websteder</toc-add-entry>

<h3 id="forums">Forummer</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <a href="http://forums.debian.net">Debian User Forums</a> are web portals
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="http://forums.debian.net">Debian User Forums</a> er en webportal 
hvor man kan diskutere Debian-relaterede emner, stille spørgsmål om Debian og få 
dem besvaret af andre brugere.</p>


<toc-add-entry name="maintainers">Kontakt en pakkes vedligeholder</toc-add-entry>

<p>Der er to måder at kontakte en pakkes vedligeholder på.  Hvis man vil i 
kontakt med en pakkes vedligeholder på grund af en programfejl, så kan man blot 
indsende en fejlrapport (se afsnittet neden for om fejlrapportering). Pakkens
vedligeholder modtager en kopi af fejlrapporten.  Husk at skrive på engelsk.</p>

<p>Hvis man ønsker at kommunikere med pakkevedligeholderen, så kan man bruge det 
særlige e-mail-alias, som er sat op for hver pakke.  Alle e-mails der sendes 
til &lt;<em>pakkenavn</em>&gt;@packages.debian.org vil blive videresendt til 
pakkevedligeholderen.</p>


<toc-add-entry name="bts" href="Bugs/">Fejlrapporteringssystemet</toc-add-entry>

<p>Debian-distributionen har et fejlrapporteringssystem som 
indeholder de detaljerede oplysninger om programfejl rapporteret af brugere og 
udviklere.  Hver enkelt fejlrapport tildeles et nummer og gemmes i arkivet 
indtil den er blevet markeret som behandlet.</p>

<p>For at rapportere en fejl, kan man læse fejlrapporteringssiden neden for, 
eller man kan bruge Debian-pakken <q>reportbug</q>, til automatisk at indsende 
en fejlrapport.</p>

<p>Oplysninger om indsendelse af fejlrapporter, oversigt over uafsluttede 
fejlrapporter, samt oplysninger om fejlrapporteringssystemet generelt, findes 
på <a href="Bugs/">websiderne om fejlrapporteringssystemet</a>.</p>


<toc-add-entry name="consultants" href="consultants/">Konsulenter</toc-add-entry>

<p>Debian er fri software og tilbyder gratis hjælp via postlister.  Nogle 
mennesker har enten ikke tid, eller har specielle behov, og er villige til 
betale nogen for at vedligeholde eller tilføje ekstra funktionalitet til deres 
Debian-system.  Se <a href="consultants/">konsulentsiderne</a> for en liste over 
personer og virksomheder, der tilbyder sådanne ydelser.</p>


<toc-add-entry name="release" href="releases/stable/">Kendte problemer</toc-add-entry>

<p>Begrænsninger og alvorlige problemer med den aktuelle stabile distribution
(om nogen) er beskrevet på <a href="releases/stable/">udgivelsessiderne</a>.</p>

<p>Vær særligt opmærksom på 
<a href="releases/stable/releasenotes">udgivelsesbemærkningerne</a> og de
<a href="releases/stable/errata">kendte fejl</a>.</p>
