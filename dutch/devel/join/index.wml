#use wml::debian::template title="Hoe u kunt meewerken"
#use wml::debian::translation-check translation="2cff2fcc9029ef0580a0e7219d3063a20c4e1294"

<p>Het Debian-project bestaat uit vrijwilligers en onze producten worden
volledig door vrijwilligers ontwikkeld. We zijn meestal <a href="$(HOME)/intro/diversity">op zoek naar nieuwe medewerkers</a> die geïnteresseerd
zijn in vrije software en wat tijd ter beschikking hebben.</p>

<p>Indien u dit nog niet deed, moet u de meeste van onze webpagina's
doornemen om een beter inzicht te krijgen in wat we proberen te doen.
Besteed in het bijzonder aandacht aan de <a href="$(HOME)/social_contract#guidelines">Debian Free Software Guidelines</a>, de
richtlijnen van Debian in verband met vrije software, en aan ons <a href="$(HOME)/social_contract">Sociaal Contract</a>.</p>

<p>Veel communicatie in het project gebeurt in onze
<a href="$(HOME)/MailingLists/">mailinglijsten</a>. Indien u voeling wil
krijgen met de interne werking van het Debian-project, moet u minstens
intekenen op de mailinglijsten debian-devel-announce en debian-news. Beide
lijsten hebben een zeer klein volume en documenteren wat gaande is in de
gemeenschap. Het Debian projectnieuws (dat gepubliceerd wordt op debian-news) vat de recente discussies samen uit mailinglijsten uit blogposts die
met Debian verband houden, en en het geeft links daarnaartoe.
Als toekomstige ontwikkelaar moet u ook intekenen op debian-mentors, een
open forum dat nieuwe onderhouders (en ook, zij het minder vaak, mensen die
nieuw zijn in het project en met iets anders dan pakketonderhoud wensen te
helpen), tracht te helpen.
Andere interessante lijsten zijn debian-devel, debian-project, debian-release, debian-qa en, afhankelijk van uw interesse, een heleboel andere.
Bekijk de pagina over <a href="$(HOME)/MailingLists/subscribe">intekenen op
mailinglijsten</a> voor een volledige lijst. (Voor wie het aantal mails
beperkt wil houden zijn er de alleen-lezen "-digest"-lijsten, samengevatte
versies van lijsten met veel verkeer. Het is ook goed om weten dat u de
pagina met <a href="https://lists.debian.org/">mailinglijstarchieven</a>
kunt gebruiken om de mails op verschillende lijsten te lezen in uw
webbrowser.)
</p>

<p><b>Zelf bijdragen.</b>
Indien u geïnteresseerd bent in het onderhouden van pakketten, moet u de
lijst van <a href="$(DEVEL)/wnpp/">Pakketten waaraan werk is en toekomstige
pakketten</a> bekijken om te zien welke pakketten een onderhouder nodig
hebben. Een onbeheerd pakket overnemen is de beste manier om als
onderhouder van start te gaan &ndash; niet enkel helpt dit Debian om zijn
pakketten goed onderhouden te houden, maar het geeft u ook de kans om te
leren van de vorige onderhouder.</p>

<p>U kunt ook helpen door bij te dragen aan het <a href="$(HOME)/doc/">schrijven van documentatie</a>, aan het <a href="$(HOME)/devel/website/">onderhoud van de website</a>, aan de <a href="$(HOME)/international/">vertaling</a> (i18n &amp; l10n), bekendmaking, juridische
ondersteuning of aan andere taken in de Debian-gemeenschap. Onze site over
<a href="https://qa.debian.org/">kwaliteitsverzekering</a> vermeldt
verschillende andere mogelijkheden.
</p>

<p>U hoeft geen officiële ontwikkelaar van Debian te zijn om elk van deze
taken uit te voeren. Bestaande ontwikkelaars van Debian die als <a
href="newmaint#Sponsor">sponsor</a> fungeren, kunnen uw werk in het project
integreren. Het is best om een ontwikkelaar te proberen vinden die actief
is in hetzelfde domein als uzelf en interesse heeft in wat u gedaan
heeft.</p>

<p>Tenslotte biedt Debian verschillende <a href="https://wiki.debian.org/Teams">ontwikkelaarsteams</a> die aan gemeenschappelijke taken
samenwerken. Iedereen kan deelnemen aan een team, al dan niet als
officiële ontwikkelaar van Debian. In een team samenwerken is een
excellente manier om ervaring op te doen voor u start met de <a
href="newmaint">procedure voor nieuwe leden</a>, en het is een van de
beste plaatsen voor het vinden van pakketsponsors. Ga dus op zoek naar een
team dat aansluit bij uw interesses en gooi u er meteen in.
</p>

<p>
<b>Aansluiten.</b>
Nadat u een tijdje bijgedragen heeft en zeker bent over uw betrokkenheid
bij het Debian-project, kunt u aansluiten bij Debian in een meer officiële
functie. Er zijn twee verschillende functies waarin u kunt aansluiten bij
Debian:
</p>

<ul>
<li>Onderhouder van Debian (Debian Maintainer - DM): De eerste stap,
waarbij u zelf uw eigen pakketten kunt uploaden naar het archief van
Debian.</li>
<li>Ontwikkelaar van Debian (Debian Developer - DD): Het traditionele
volledige lidmaatschap van Debian. Een DD kan deelnemen aan verkiezingen in
Debian.
DD's met uploadbevoegdheid kunnen om het even welk pakket uploaden
naar het archief. Voor u solliciteert voor de functie van DD met
uploadbevoegdheid moet u minstens gedurende zes maanden een staat van
dienst hebben als onderhouder van pakketten. Dit kan in de hoedanigheid van
DM die eigen pakketten uploadt, of via het werken in een team, of via het
onderhouden van pakketten die door een sponsor geüpload worden.
DD's zonder uploadbevoegdheid hebben dezelfde verpakkingsbevoegdheid als
onderhouders van Debian. Voor u solliciteert voor de functie van DD zonder
uploadbevoegdheid moet u een zichtbare en significante staat van dienst
hebben als medewerker binnen het project.</li>

</ul>

<p>Ondanks het feit dat veel van de bevoegdheden en verantwoordelijkheden
van een DM en een DD identiek zijn, bestaan er momenteel onafhankelijke
procedures om te solliciteren voor elk van deze functies. Raadpleeg de <a
href="https://wiki.debian.org/DebianMaintainer">wikipagina voor
onderhouders van Debian</a> als u een onderhouder van Debian wil worden. En
raadpleeg de <a href="newmaint">hoek voor nieuwe leden</a> om te weten hoe
u moet solliciteren voor de functie van officiële ontwikkelaar van Debian.
</p>

<p>Merk op dat gedurende een lange periode in de geschiedenis van Debian
de functie van ontwikkelaar van Debian de enige functie was. De functie
van onderhouder van Debian werd ingevoerd op 5 augustus 2007. Dit is de
reden waarom u de term "onderhouder" in een historische zin gebruikt ziet,
terwijl de term ontwikkelaar preciezer zou zijn. De procedure om te
solliciteren voor ontwikkelaar van Debian stond nog tot 2011 bekend als de
procedure "Debian New Maintainer" (nieuwe onderhouder van Debian) en werd
toen hernoemd naar "Debian New Member" procedure, de procedure voor nieuwe
leden van Debian.</p>

<p>Voor welke functie u ook solliciteert, u moet vertrouwd zijn met de
werkwijzes van Debian en daarom is het aangewezen om het <a
href="$(DOC)/debian-policy/">Debian Beleidshandboek</a> (Debian Policy) en
de <a href="$(DOC)/developers-reference/">Referentiehandleiding voor
ontwikkelaars</a> (Developer's Reference) te lezen voor u solliciteert.</p>

<p>Samen met de vele ontwikkelaars, kunt ook u
<a href="$(HOME)/intro/help">Debian helpen</a> met onder andere testen,
documentatie, geschikt maken van Debian voor andere architecturen,
<a href="$(HOME)/donations">giften</a> in de vorm van geld of het ter
beschikking stellen van machines voor ontwikkelingswerk of connectiviteit.
In sommige delen van de wereld zijn we permanent op zoek naar
<a href="$(HOME)/mirror/">spiegelservers</a>.</p>

